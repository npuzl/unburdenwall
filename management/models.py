from django.db import models

# Create your models here.
from publish.models import PublishForm


class GeneratedPic(models.Model):
    gid = models.AutoField(primary_key=True)
    form = models.ForeignKey(to=PublishForm, on_delete=models.CASCADE)
    pic = models.ImageField(upload_to='pics/gen/')
    time = models.DateTimeField(auto_now_add=True)
    send = models.BooleanField(default=False)

    class Meta:
        db_table = "GeneratedPic"

    def __str__(self):
        return str(self.form)

class Statistics(models.Model):
    sid=models.AutoField(primary_key=True)
    date=models.DateField() #日期
    passed=models.IntegerField(default=0) #同意
    rejected=models.IntegerField(default=0) #拒绝
    undefined=models.IntegerField(default=0)  #未审核
    visitors=models.IntegerField(default=0)  #今日访问量
    sum=models.IntegerField(default=0)  #从系统上线以来全部表单量

    def __str__(self):
        return "{0}|{1}|{2}|{3}|{4}".format(str(self.date),self.passed,self.rejected,self.undefined,self.visitors)

    class Meta:
        db_table = "statistices"
        ordering = ["-date"]