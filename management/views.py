# Create your views here.
import datetime
import json

from django.forms.models import model_to_dict
from django.http import HttpResponse
from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from management.models import Statistics
from publish import models


@api_view(['POST'])
def checkpic(requset):
    fid = requset.data.get('fid')
    check = requset.data.get('result')
    try:
        obj = models.PublishForm.objects.get(fid=fid)
        data = timezone.now().date()
        try:
            today = Statistics.objects.get(date=data)
        except:
            today = Statistics.objects.create(date=data)
        if check:
            obj.status = 1
            obj.save()
            today.passed += 1
        else:
            obj.status = -1
            obj.save()
            today.rejected += 1
        today.undefined -= 1
        today.save()
        return Response({"success": True}, status=status.HTTP_200_OK)
    except:
        return Response({"success": False}, status=status.HTTP_404_NOT_FOUND)


class DateEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.strftime("%Y-%m-%d %H:%M:%S")
        else:
            return json.JSONEncoder.default(self, obj)


@api_view(['GET'])
def checklist(requset):
    model = models.PublishForm.objects.all().filter(status=0).exclude(title="")
    json_list = []
    # data = serializers.serialize('json', models.PublishForm.objects.all(), fields=('fid', 'content'))
    # print(data)

    for i in model:
        json_dict = model_to_dict(i)
        json_dict["time"] = i.time
        del json_dict["ip"]
        pics_set = models.Picture.objects.all().filter(fid=i.fid)
        pics = []
        for j in pics_set:
            pics.append(str(j.pic))
        json_dict['pic'] = pics
        # json_dict["pic"] = pics
        json_list.append(json_dict)

    return HttpResponse(json.dumps(json_list, cls=DateEncoder))


@api_view(['GET'])
def pic(requset):
    pid = requset.GET.get('pid')
    try:
        model = models.Picture.objects.get(pid=pid)
        data = str(model.pic)
        return HttpResponse(data)
    except:
        print("没有这张图")
        return Response({}, status=404)


@api_view(['GET'])
def get_item(requset):
    fid = requset.GET.get("fid")
    try:
        model = models.PublishForm.objects.all().get(fid=fid)
        json_dict = model_to_dict(model)
        json_dict["time"] = model.time
        pics_set = models.Picture.objects.all().filter(fid=fid)
        pics = []
        for j in pics_set:
            pics.append(str(j.pic))
        json_dict['pics'] = pics
        return HttpResponse(json.dumps(json_dict, cls=DateEncoder))
    except:
        return Response({}, status=404)


@api_view(['GET'])
def thumbpic(request):
    pid = request.GET.get("pid")
    try:
        model = models.Thumb.objects.all().get(pic=pid)
        return Response(str(model.thumb))
    except:
        return Response({}, status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def all(requset):
    try:
        page = requset.GET.get("page")
    except:
        page = 1
    model = models.PublishForm.objects.all().exclude(title="")
    json_list = []
    data = {}
    # data = serializers.serialize('json', models.PublishForm.objects.all(), fields=('fid', 'content'))
    # print(data)

    for i in model:
        json_dict = model_to_dict(i)
        json_dict["time"] = i.time
        pics_set = models.Picture.objects.all().filter(fid=i.fid)
        pics = []
        for j in pics_set:
            pics.append(str(j.pic))
        json_dict['pic'] = pics
        # json_dict["pic"] = pics
        json_list.append(json_dict)
    # size = len(json_dict)
    # count = size // 20
    # if size % 20 != 0:
    #     count = count + 1
    data["count"] = len(json_list)
    if (int(page) - 1) * 20 > len(json_list):
        return HttpResponse(json.dumps({'success': False}), status=status.HTTP_404_NOT_FOUND)
    if page == "1":
        data["previous"] = ""
    else:
        data["previous"] = "http://192.144.227.111:8000/api/list/all/?page=" + str(int(page) - 1)
    print(page)
    next_page = int(page) + 1
    print(next_page)

    next = 'http://192.144.227.111:8000/api/list/all/?page=' + str(next_page)
    if int(page) * 20 < len(json_list):
        data["next"] = next
    else:
        data["next"] = ""
    data["results"] = json_list[(int(page) - 1) * 20:int(page) * 20]
    print(data)
    return HttpResponse(json.dumps(data, cls=DateEncoder))

@api_view(['GET'])
def getStatistics(requset):
    if requset.method=='GET':
        res = {'success': True}
        try:
            data=Statistics.objects.all()
            if len(data)>14:
                data=data[0: 14]
            out=[]
            for d in data:
                out.append({
                    'date':"{0}-{1}-{2}".format(d.date.year,d.date.month,d.date.day),
                    'passed':d.passed,
                    'rejected':d.rejected,
                    'undefined':d.undefined,
                    'visitors':d.visitors,
                    'sum':d.sum
                })
            res['data'] = out
            print(res)
            print("\n\n\n\n")
            return HttpResponse(json.dumps(res),status=status.HTTP_200_OK)
        except:
            res['success']=False
            return HttpResponse(json.dumps(res), status=status.HTTP_404_NOT_FOUND)
