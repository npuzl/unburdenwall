from django.contrib import admin

# Register your models here.
from management.models import GeneratedPic,Statistics

admin.site.register(GeneratedPic)
admin.site.register(Statistics)