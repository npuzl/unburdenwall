from django.contrib import admin
from django.urls import path, include

from publish import views

urlpatterns = [
    path('',views.FormPage,name='form'),
    path('publish/', views.publishFormPage, name="publishform")
]
