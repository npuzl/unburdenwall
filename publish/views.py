import datetime
import json

from django.http import JsonResponse
# Create your views here.
from django.utils import timezone
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet

from management.models import Statistics
from publish.models import PublishForm, Picture
from publish.serializers import FormMakeSerializer, PictureSerializer, FormSerializer


@api_view(['GET'])
def FormPage(request):
    if request.method == "GET":
        form = PublishForm.objects.create()
        date=timezone.now().date()
        yesterday = Statistics.objects.get(date=date - datetime.timedelta(hours=23, minutes=59, seconds=59))
        try:
            today=Statistics.objects.get(date=date)
        except:
            today=Statistics.objects.create(date=date)

        today.visitors+=1
        today.sum=yesterday.sum
        today.save()
        form = FormMakeSerializer(form)
        return JsonResponse(form.data, safe=False)
    elif request.method == "POST":
        print(request)
        res = FormSerializer(request)
        return JsonResponse(res.data, safe=False)


class PictureViewSet(viewsets.ModelViewSet):
    queryset = Picture.objects.all()
    serializer_class = PictureSerializer
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        file = request.data['pic']
        size = file.size
        response = super(ModelViewSet, self).create(request, *args, **kwargs)
        return response


@api_view(['POST'])
def publishFormPage(request):
    if request.method == "POST":
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')  # 判断是否使用代理
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]  # 使用代理获取真实的ip
        else:
            ip = request.META.get('REMOTE_ADDR')  # 未使用代理获取IP
        jsonBody = json.loads(request.body.decode('utf-8'))
        jsonBody['time'] = str(timezone.now())
        jsonBody['ip'] = str(ip)
        jsonBody['status']=0
        ser = FormSerializer(data=jsonBody)
        # print(jsonBody)
        success = {'success': True, 'msg': 'success'}
        fail = {'success': False, 'msg': 'error'}
        try:
            form =PublishForm.objects.get(fid=jsonBody['fid'])
            form.type=jsonBody["type"]
            form.title=jsonBody["title"]
            form.content=jsonBody["content"]
            form.contact=jsonBody["contact"]
            form.time=jsonBody['time']
            form.ip=jsonBody["ip"]
            form.status=0
            form.save()
            # print(form)
            # print(jsonBody)
            if ser.is_valid():
                data = timezone.now().date()
                try:
                    today = Statistics.objects.get(date=data)
                except:
                    today = Statistics.objects.create(date=data)
                today.sum+=1
                today.undefined+=1
                today.save()
                return JsonResponse(success, safe=False,status=201)
            else:
                fail['msg']='data is not valid'
                return JsonResponse(fail,safe=False,status=404)
        except Exception as e:
            fail['msg']="form id error"
            return JsonResponse(fail,safe=False,status=404)


