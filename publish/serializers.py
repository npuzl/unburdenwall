from django.utils import timezone
from rest_framework import serializers

from publish.models import PublishForm, Picture


class FormSerializer(serializers.ModelSerializer):
    class Meta:
        model = PublishForm
        fields = [
            'fid',
            'ip',
            'time',
            'type',
            'title',
            'content',
            'contact',
            'status'
        ]

    def create(self, validated_data):
        print(validated_data)
        PublishForm.objects.get()
        return None


class FormMakeSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    fid = serializers.IntegerField()

    class Meta:
        model = PublishForm
        fields = [
            'fid'
        ]


class PictureSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='picture-detail')

    class Meta:
        model = Picture
        fields = '__all__'

    def create(self, validated_data):
        print(validated_data)
        pic = Picture.objects.create(
            fid=validated_data['fid'],
            pic=validated_data['pic'],
            uptime=timezone.now()
        )
        pic.size = validated_data['pic'].size
        return pic
