from django.contrib import admin
# Register your models here.
from django.contrib.auth.admin import UserAdmin
#
from userprofile.models import Checker

ADDITIONAL_FIELDS = ((None, {'fields': ('nickname', 'phone', 'qq')}),)


class MyUserAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets + ADDITIONAL_FIELDS
    add_fieldsets = UserAdmin.fieldsets + ADDITIONAL_FIELDS


admin.site.register(Checker, MyUserAdmin)
