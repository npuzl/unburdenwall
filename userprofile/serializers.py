from rest_framework import serializers
from userprofile.models import Checker


class CheckerSerializer(serializers.ModelSerializer):
    # nickname = serializers.CharField(max_length=30, default="未设置昵称", blank=True, null=True)
    # phone = serializers.CharField(max_length=11, default="12345678901", blank=True, null=True)
    # qq = serializers.CharField(max_length=11, default="", blank=True, null=True)

    class Meta:
        model = Checker
        fields = [
            'id',
            'username',
            'nickname',
            'password',
            'qq',
            'phone',
        ]
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        user = Checker.objects.create_user(**validated_data)
        return user

    def update(self, instance, validated_data):
        if 'password' in validated_data:
            password = validated_data.pop("password")
            instance.set_password(password)
        return super().update(instance, validated_data)


class CheckerDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Checker
        fields = [
            'id',
            'username',
            'last_name',
            'first_name',
            'email',
            'phone',
            'last_login',
            'date_joined'
        ]
